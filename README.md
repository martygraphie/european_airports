
# Graphical representation of European air interconnections

## Implementation

## Data recovery

### Open flights site
The data set is not up to date, but the site makes available 2017 data for free.

### Airport lists
File airports.dat

### Air routes lists
File routes.dat

### Filtering
Worldwide data filtered on European routes and airports, airports without IATA codes are removed from the data to be analyzed

### Conversion
Data requiring manipulation have been converted into the appropriate formats.

![Data Visualisation](european_airports_hub.png)
